(ns book-exchange.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [book-exchange.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[book-exchange started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[book-exchange has shut down successfully]=-"))
   :middleware wrap-dev})
