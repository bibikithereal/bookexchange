(ns book-exchange.crawler.crawler
  (:import (org.jsoup Jsoup)
           (org.jsoup.select Elements)
           (org.jsoup.nodes Element))
  (:require [overtone.at-at :as at]
            [book-exchange.db.core :as db]
            [clojure.tools.logging :as log]
            [mount.core :refer [defstate]]))

(def telegrafi "https://telegrafi.com")

(def main-article-selector "body > div.aktuale-widget.container > div > div.col-md-16.col-sm-24.col-xs-24.pt0 > div.aktuale-v4 > div > div.col-sm-15.col-xs-24 > a")

(def main-article-text-selector ".article-body")

(def scheduler-pool (at/mk-pool))

(defn get-page [url]
  (.get (Jsoup/connect url)))

(defn get-attr [elem attr]
  (.attr elem attr))

(defn get-text [elem]
  (.text elem))

(defn get-own-text [elem]
  (.ownText elem))

(defn get-elem [elem cssSelector]
  (.select elem cssSelector))

(defn telegrafi-crawler []
  "Goes to telegrafi.com, gets the link to the main article, retrieves words from the article and returns that as a result."
  (println "Running telegrafi crawler")
  (log/info "Running telegrafi crawler")
  (-> telegrafi
      (get-page)
      (get-elem main-article-selector)
      (get-attr "href")
      (get-page)
      (get-elem main-article-text-selector)
      (get-text)
      (clojure.string/split #" ")))

(defn crawl [crawler]
  (doseq [word (crawler)] (if (not (db/find-word {:word word})) (db/create-word! {:word word}))))

(defn schedule [func pool]
  (println "Scheduling crawler: " func)
  (log/info "Scheduling crawler: " func)
  (at/every 1200000 func pool))

(defstate crawler
  :start (schedule #(crawl telegrafi-crawler) scheduler-pool)
  :stop #(at/stop crawler))
