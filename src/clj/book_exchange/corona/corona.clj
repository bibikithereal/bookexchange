(ns book-exchange.corona.corona
  (:require [cheshire.core :refer [parse-string]]
            [overtone.at-at :as at]))

(def data "https://covid19.mathdro.id/api")

(defn my-func []
  (let [string-json (slurp data)
        json-map (parse-string string-json)
        casualties (get (get json-map "deaths") "value")
        recovered (get (get json-map "recovered") "value")
        confirmed (get (get json-map "confirmed") "value")]
    {:casualties casualties
     :effective-rate (double (/ casualties (+ casualties recovered) 1/100))
     :total-rate (double (/ casualties confirmed 1/100))}))
