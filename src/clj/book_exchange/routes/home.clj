(ns book-exchange.routes.home
  (:require
   [book-exchange.layout :as layout]
   [book-exchange.db.core :as db]
   [clojure.java.io :as io]
   [book-exchange.routes.blog.home :as blog]
   [book-exchange.routes.corpus.corpus :as corpus]
   [book-exchange.routes.hicc.home :as hicc]
   [book-exchange.middleware :as middleware]
   [ring.util.http-response :as response]
   [ring.middleware.multipart-params :refer [wrap-multipart-params]]
   [ring.middleware.params :refer [wrap-params]]
   [struct.core :as struct]
   [buddy.auth :refer [authenticated? throw-unauthorized]]
   [buddy.hashers :as hashers]
   [clojure.tools.logging :as log]))

(defn- validate [params schema]
  (first (struct/validate params schema)))

(def books-schema
  [[:author struct/required struct/string]
   [:title struct/required struct/string]])

(defn validate-book [params]
  (validate params books-schema))

(defn home-page [request]
  (layout/render request "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn about-page [request]
  (layout/render request "about.html"))

(defn template-page [request]
  "I use this to see what is available in my template"
  (layout/render request "template.html"))

(defn log-in [request]
  (layout/render request "login.html"))

(defn log-out [request]
  (log/debug "loging user out: " (get-in request [:session :identity]))
  (assoc (response/found "/login") :session {}))

(defn register [request]
  (log/debug "showing register page")
  (layout/render request "register.html"))

(defn register-user [request]
  (let [new-user (:params request)]
    (-> new-user
        (assoc :password (hashers/derive (:password new-user)))
        (db/create-user!))
    (response/found "/login")))

(defn authenticate [request]
  (let [username (:username (:params request))
        password (:password (:params request))
        user (db/get-user {:username username})]
    (if (hashers/check password (:password user))
      (assoc (response/found "/books") :session {:identity username})
      (do (log/debug "wrong password provided for user " username)
        (layout/render request "login.html" {:message "Please try again. Apparently you entered a wrong password."})))))

(defn books [request]
  (if (not (authenticated? request))
    (do (log/debug "non authenticated request")
        (response/found "/login"))
    (layout/render request "books.html" {:books (db/get-books)})))

(defn books-add [request]
  (if-let [errors (validate-book (:params request))]
    (layout/render request "books.html" {:flash errors})
    (do
      (db/create-book! (:params request))
      (layout/render request "books.html" {:books (db/get-books) :flash "Book added successfully"}))))

;;this here is reitit way of routing - it is different than ring and compojure
(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats
                 middleware/wrap-auth]}
   ["/words/fromtext" {:post (wrap-multipart-params (wrap-params corpus/upload-file))}]
   ["/words" {:get corpus/words-home}]
   ["/" {:get home-page}]
   ["/about" {:get about-page}]
   ["/login" {:get log-in}]
   ["/authenticate" {:post authenticate}]
   ["/logout" {:get log-out}]
   ["/template" {:get template-page}]
   ["/books" {:get books}]
   ["/books/add" {:post books-add}]
   ["/register" {:get register}]
   ["/blog" {:get blog/add-blog-page}]
   ["/blog/add" {:post blog/add-blog}]
   ["/hicc" {:get hicc/home}]
   ["/register/user" {:post register-user}]])
