(ns book-exchange.routes.corpus.corpus
  (:require
   [book-exchange.layout :as layout]
   [book-exchange.db.core :as db]
   [buddy.auth :refer [authenticated? throw-unauthorized]]
   [ring.util.http-response :as response]))

(defn words-home [request]
  (layout/render request "corpus/words-home.html" {:words (db/get-words {:x 10})}))

(defn upload-file [request]
  (println "myfile" (:params request) "\n" request)
  (layout/render request "corpus/words-home.html" {:flash "Your file has been received successfully and is being processed."}))
