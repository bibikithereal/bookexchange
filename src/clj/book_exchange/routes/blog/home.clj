(ns book-exchange.routes.blog.home
  (:require
   [book-exchange.layout :as layout]
   [book-exchange.db.core :as db]
   [clojure.java.io :as io]
   [book-exchange.middleware :as middleware]
   [ring.util.http-response :as response]
   [struct.core :as struct]
   [buddy.auth :refer [authenticated? throw-unauthorized]]
   [buddy.hashers :as hashers]
   [clojure.tools.logging :as log]))

(defn add-blog-page [request]
  (if (not (authenticated? request))
    (response/found "/login")
    (layout/render request "blog/add-blog.html")))

(defn add-blog [request]
  (if (not (authenticated? request))
    (response/found "/login")
    (do
      (-> request
          (:params)
          (select-keys [:title :content])
          (merge {:author (get-in request [:session :identity]) :published_at (java.time.LocalDateTime/now)})
          (db/create-blog!))
      (response/found "/blog"))))
