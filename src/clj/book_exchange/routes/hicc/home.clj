(ns book-exchange.routes.hicc.home
  (:require
   [ring.util.http-response :refer [content-type ok]]
   [book-exchange.views.h :as view]
   [book-exchange.layout :as layout]))

(defn home [request]
  (content-type (ok (view/empty-view)) "text/html; charset=utf-8")
  )
