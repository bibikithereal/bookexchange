(ns book-exchange.views.h
  (:require
   [hiccup.core :refer [html h]]
   [reagent.core :as r]))

;(def click-count (r/atom 0))





(defn simple-component []
  [:div
   [:p "I am a component!"]
   [:p.someclass
    "I have " [:strong "bold"]
    [:span {:style {:color "red"}} " and red "] "text."]])

(defn simple-parent []
  [:div
   [:p "I include simple-component."]
   [simple-component]])


(defn empty-view []
  (html (simple-parent)))
