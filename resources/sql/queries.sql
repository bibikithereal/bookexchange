-- :name create-book! :! :n
-- :doc creates a new book record
INSERT INTO books (author, title) VALUES (:author, :title);

-- :name get-books
-- :doc retrieves all book records
SELECT * FROM books;

-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users (username, password, firstname, lastname) VALUES (:username, :password, :firstname, :lastname);

-- :name get-user :? :1
-- :doc retrieves a user by id
SELECT * FROM users WHERE username = :username;

-- :name create-blog! :! :n
-- :doc creates a new blog record
INSERT INTO blogs (title, content, author, published_at) VALUES (:title, :content, :author, :published_at);

-- :name create-word! :! :n
-- :doc creates a new word record
INSERT INTO words (word) VALUES(:word);

-- :name get-words
-- :doc select x many words
SELECT * FROM WORDS LIMIT :x;

-- :name find-word :? :1
-- :doc selects a word by its value
SELECT * FROM WORDS WHERE word = :word;
