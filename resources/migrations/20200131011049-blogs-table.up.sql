CREATE TABLE blogs (
id serial PRIMARY KEY,
title VARCHAR(90),
content VARCHAR(2200),
author VARCHAR(50));
