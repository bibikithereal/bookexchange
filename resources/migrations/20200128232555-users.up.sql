CREATE TABLE users
(id serial PRIMARY KEY,
username VARCHAR(50),
password VARCHAR(100),
firstname VARCHAR(50),
lastname VARCHAR(50));
