CREATE TABLE books
(id serial PRIMARY KEY,
author VARCHAR(50),
title VARCHAR(100),
description VARCHAR(1000));
