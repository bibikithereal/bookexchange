CREATE TABLE books
(id serial PRIMARY KEY,
author VARCHAR(50),
title VARCHAR(100),
description VARCHAR(1000));

--;;
CREATE TABLE users
(id serial PRIMARY KEY,
username VARCHAR(50),
password VARCHAR(64));
